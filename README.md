# devops-engineer-challenge

## Architecture 

![This is an image](https://gitlab.com/Prietno/devops-engineer-challenge/-/blob/6445e4c24c8b845f098510d86e256d8a1470da40/photo_2022-12-07_17.03.09.jpeg)


## Workflow 

![This is an image](https://gitlab.com/Prietno/devops-engineer-challenge/-/blob/6445e4c24c8b845f098510d86e256d8a1470da40/photo_2022-12-07_17.03.14.jpeg)



## Getting started
### Fullstack JS (reactjs, expressjs, postgresql)
Simple CRUD app built with ReactJS, ExpressJS, PostgreSQL.
```
Database    : postgresql
Backend     : localhost:8080
Frontend    : localhost:8081
```

## Setup

### Database

#### Docker
- [x]  Build database custom image
- [x]  Install psql client
- [x]  Check data

- `docker build -f /path/Dockerfile.dbstag -t postgresql-binar:v.1.0.0`
- `sudo apt-get install postgresql-client`
- `psql -p 5432 -h 34.xxx.xxx.xxx -U people -W people`

#### CloudSQL
- [x]  Go to GCP and choose SQL
- [x]  Create an instance and choose postgresql
- [x]  Setup Authorized networks, input you ip allow 34.xxx.xxx.xxx/32
- [x]  Check data


### VM's Instance
- [x]  Go to GCP and choose Compute engine
- [x]  Define 3 instance: 1. Gitlab Runner, 2. Stagging, 3. Production
- [x]  Setting ssh between each instance

    -  `ssh-keygen -C academydataengineer`
    -  `cat /path/.ssh/id_rsa.pub`
    -  copy and paste to instance you want to connect in path `sudo nano .ssh/authorized_keys`
- [x]  test connect ssh using command `ssh academydataengineer@34.xxx.xxx.xxx`
- [x]  loop through all instances until all id_rsa.pub are listed


### Backend

#### Stagging
Create .env file. Configure with appropriate value.
```
cd backend
cp .env.example .env
```

Simply install all dependencies then start the app. And you can start with this command. You'll see the server is running on port 8080
```
npm install
npm start
```

In this project we can simply with running docker command
```
docker build -t rudyprietno/deo-engineer-challenge:be-tag -f stag/backend/Dockerfile.stag ./stag/backend
```


#### Production
In this project we can simply with running docker command
```
docker build -t rudyprietno/deo-engineer-challenge:be-tag -f prod/backend/Dockerfile.prod ./prod/backend
```


### Frontend

#### Stagging
Create .env file. Configure with appropriate value.
```
cd frontend
cp .env.example .env
```

Simply install all dependencies then start the app. And you can start with this command. You'll see the server is running on port 8080
```
npm install
npm start
```

In this project we can simply with running docker command
```
docker build -t rudyprietno/deo-engineer-challenge:be-tag -f stag/frontend/Dockerfile.stag ./stag/frontend
```


#### Production
In this project we can simply with running docker command
```
docker build -t rudyprietno/deo-engineer-challenge:be-tag -f prod/frontend/Dockerfile.prod ./prod/frontend
```


## Rapid Execution

### Backend, Database, Frontend on Stagging level
```
docker compose -f /path/stag/docker-compose-staging.yml up -d
```

### Backend, Database, Frontend on Production level
```
docker compose -f /path/prod/docker-compose-staging.yml up -d
```

## Result
- [x]  Production
```
https://api-prod.aktifitasacak.com/users -- Backend
https://prod.aktifitasacak.com/ -- Frontend

```

- [x]  Stagging
```
https://api-stag.aktifitasacak.com/users -- Backend
https://stag.aktifitasacak.com/ -- Frontend

```


## Additional


### Setup Nginx and SSL (Certbot)
- [x]  Define Willcard SSL
```
certbot certonly \
--agree-tos --manual --preferred-challenges dns \
--email random@aktifitasacak.com \
--server https://acme-v02.api.letsencrypt.org/directory \
-d "*.aktifitasacak.com"
```

- [x]  Go to Cloud DNS and Setup DNS TXT

When you run - Define Willcard SSL you will get info like this:
```
Please deploy a DNS TXT record under the name:
    _acme-challenge.aktifitasacak.com.
with the following value:
    N0fqg1Yzt-C236ys6ZU6-X-YwmvQ0sSs3H70aiaia
```

- add record set 
    - DNS name input: _acme-challenge
    - Resource record type: N0fqg1Yzt-C236ys6ZU6-X-YwmvQ0sSs3H70aiaia

- [x]  Done the process
- [x]  run this ```sudo certbot certificates``` for check the certificate



## SSL Problem

### How to copy certificate from server staging to production?


- [x]   Setup Willcard by certbot-dns-google

```
sudo snap set certbot trust-plugin-with-root=ok && \
sudo snap install certbot-dns-google && \
sudo certbot certonly --dns-google --dns-google-credentials certbot.json -d *.aktifitasacak.com
```

- [x]   Copy fullchain.pem 
Go to server staging

```
sudo su
cd /etc/letsencrypt/
cat fullchain.pem -- copy the result
```

go to server production 
```
sudo su
cd /etc/letsencrypt/ 
nano fullchain.pem -- and put the copy
```


- [x]   Copy ssl-dhparams.pem 
Go to server staging

```
sudo su
cd /etc/letsencrypt/
cat ssl-dhparams.pem -- copy the result
```

go to server production 
```
sudo su
cd /etc/letsencrypt/ 
nano ssl-dhparams.pem -- and put the copy
```
