# !/bin/bash
# This script is intended to setup the environment for a NGINX Web Server with SSL certificate using Let's Encrypt.


# install nginx
install_nginx(){
    sudo apt-get update && \
    sudo apt-get install nginx snapd -y
    # Get latest updates
    echo ""
    echo "UPDATING SYSTEM & SETUP NGINX..."
    echo ""
}


# install certbot
install_certbot(){
    sudo apt-get update && \
    sudo snap install core; sudo snap refresh core && \
    sudo snap install --classic certbot && \
    sudo ln -s /snap/bin/certbot /usr/bin/certbot
    echo ""
    echo "UPDATING SYSTEM & SETUP CERTBOT..."
    echo ""
}



# setup reverse proxy & ssl
# variable
DOMAINFE="stag.aktifitasacak.com"
DOMAINBE="api.stag.aktifitasacak.com"
NGINX=$(sudo nginx -v)
CERTBOT=$(sudo certbot --version)
CERTIFICATEFE=$(sudo certbot certificates | grep $DOMAINFE)
CERTIFICATEBE=$(sudo certbot certificates | grep $DOMAINBE)
PROJECT_DIR_NGINX_AVAILABLE="/etc/nginx/sites-available"
PROJECT_DIR_NGINX_ENABLED="/etc/nginx/sites-enabled"


# frontend
frontend_ssl() {
    sudo rm -rf ${PROJECT_DIR_NGINX_AVAILABLE}/stag.aktifitasacak.com
    sudo rm -rf ${PROJECT_DIR_NGINX_ENABLED}/stag.aktifitasacak.com

    sudo touch ${PROJECT_DIR_NGINX_AVAILABLE}/stag.aktifitasacak.com

    echo 'server {
                server_name stag.aktifitasacak.com;
                
                access_log /var/log/nginx/stag.aktifitasacak.com-access.log;
                error_log /var/log/nginx/stag.aktifitasacak.com-error.log;

                            location / {
                                proxy_set_header Host $host;
                                proxy_set_header X-Real-IP $remote_addr;
                                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                                proxy_pass http://localhost:8081;
                            }
                        

                listen 443 ssl; # managed by Certbot
                ssl_certificate /etc/letsencrypt/live/aktifitasacak.com/fullchain.pem; # managed by Certbot
                ssl_certificate_key /etc/letsencrypt/live/aktifitasacak.com/privkey.pem; # managed by Certbot
                include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
                ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

            }
            server {
                if ($host = stag.aktifitasacak.com) {
                    return 301 https://$host$request_uri;
                } # managed by Certbot


                            server_name stag.aktifitasacak.com;
                listen 80;
                return 404; # managed by Certbot


            }' | sudo tee ${PROJECT_DIR_NGINX_AVAILABLE}/stag.aktifitasacak.com

    # copy with symlink
    sudo ln -s ${PROJECT_DIR_NGINX_AVAILABLE}/stag.aktifitasacak.com ${PROJECT_DIR_NGINX_ENABLED}/

    # check nginx
    sudo nginx -t

    # restart nginx 
    sudo systemctl restart nginx
}


# backend
backend_ssl() {
    sudo rm -rf ${PROJECT_DIR_NGINX_AVAILABLE}/api.stag.aktifitasacak.com
    sudo rm -rf ${PROJECT_DIR_NGINX_ENABLED}/api.stag.aktifitasacak.com

    sudo touch ${PROJECT_DIR_NGINX_AVAILABLE}/api.stag.aktifitasacak.com

    echo 'server {
                server_name api-stag.aktifitasacak.com;
                
                access_log /var/log/nginx/api.stag.aktifitasacak.com-access.log;
                error_log /var/log/nginx/api.stag.aktifitasacak.com-error.log;

                location / {
                    proxy_set_header Host $host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_pass http://localhost:8080;
                }
            

                listen 443 ssl; # managed by Certbot
                ssl_certificate /etc/letsencrypt/live/aktifitasacak.com/fullchain.pem; # managed by Certbot
                ssl_certificate_key /etc/letsencrypt/live/aktifitasacak.com/privkey.pem; # managed by Certbot
                include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
                ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

            }
            server {
                if ($host = api-stag.aktifitasacak.com) {
                    return 301 https://$host$request_uri;
                } # managed by Certbot


                listen 80;
                return 404; # managed by Certbot


            }' | sudo tee ${PROJECT_DIR_NGINX_AVAILABLE}/api.stag.aktifitasacak.com

    # copy with symlink
    sudo ln -s ${PROJECT_DIR_NGINX_AVAILABLE}/api.stag.aktifitasacak.com ${PROJECT_DIR_NGINX_ENABLED}/

    # check status nginx config
    sudo nginx -t

    # restart nginx 
    sudo systemctl restart nginx
}


# run function
install_nginx
install_certbot
backend_ssl
frontend_ssl


echo ""
echo "INSTALLING NGINX && SSL DONE"
echo ""